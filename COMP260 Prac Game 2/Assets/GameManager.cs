﻿using UnityEngine;
using System.Collections;

public class GameManager : MonoBehaviour {

    public enum GameState
    {
        Playing = 1,
        Over = 2
    }

    private GameState state;


    private GameObject scorekeeper;


    static GameManager manager = null;

    public static GameManager GetInstance()
    {
        if(manager == null)
        {
            manager = GameObject.Find("GManager").GetComponent<GameManager>();
        }

        return manager;
    }

    public GameState GetGameState()
    {
        return state;
    }

    public void SetGameState(GameState state)
    {
        this.state = state;

        switch(state)
        {
            case GameState.Over:
                break;
            case GameState.Playing:
                break;
        }

    }

	// Use this for initialization
	void Start () {

        scorekeeper = GameObject.Find("Scorekeeper");

	}
	
	// Update is called once per frame
	void Update () {
	    if(state == GameState.Over)
        {
            if(Input.GetMouseButtonDown(0))
            {
                //reset.
                if(scorekeeper == null)
                {
                    scorekeeper = GameObject.Find("Scorekeeper");
                }
                scorekeeper.GetComponent<Scorekeeper>().GameReset();

                state = GameState.Playing;
                

            }


        }
	}
}
