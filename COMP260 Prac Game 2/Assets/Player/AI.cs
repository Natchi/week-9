﻿using UnityEngine;
using System.Collections;

public class AI : MonoBehaviour {

    public GameObject Puck;

    private Rigidbody rb;

    public float speed = 40;
    
    // Use this for initialization
    void Start () {
        Puck = GameObject.FindGameObjectWithTag("Puck");

        rb = GetComponent<Rigidbody>();

	}
	
	// Update is called once per frame
	void Update () {


       


        Vector3 pos = Puck.transform.position - this.transform.position;
        pos.Normalize();
        pos.Scale(Vector3.up);
        rb.velocity = pos * speed;
        
	}

    void OnCollisionEnter(Collision collision)
    {

    }
}
