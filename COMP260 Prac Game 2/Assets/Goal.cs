﻿using UnityEngine;
using System.Collections;

public class Goal : MonoBehaviour {
    public delegate void ScoreGoalHandler(int player);
    public ScoreGoalHandler scoreGoalEvent;
    public int player;
    public AudioClip scoreClip;
    private AudioSource audio;
	// Use this for initialization
	void Start () {
        audio = GetComponent<AudioSource>();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnTriggerEnter (Collider collider)
    {
        audio.PlayOneShot(scoreClip);

        puckControl puck =
            collider.gameObject.GetComponent<puckControl>();
        puck.ResetPosition();

        if (scoreGoalEvent != null)
        {
            scoreGoalEvent(player);
        }
    }

    

  
}
