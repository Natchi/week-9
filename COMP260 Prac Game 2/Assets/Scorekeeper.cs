﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Scorekeeper : MonoBehaviour {
    public int scorePerGoal = 1;
    private int[] score = new int[2];
    public Text[] scoreText;

    public GameObject TextPrefab;

    public GameObject Canvas;

    private GameObject WinnerText = null;


	// Use this for initialization
	void Start () {


        Goal[] goals = FindObjectsOfType<Goal>();

        for (int i = 0; i < goals.Length; i++)
        {
            goals[i].scoreGoalEvent += OnScoreGoal;
        }

        for (int i =0; i < score.Length; i++)
        {
            score[i] = 0;
            scoreText[i].text = "0";
        }
	}

    public void GameReset()
    {
        if (WinnerText != null)
        {
            GameObject.Destroy(WinnerText);
        }

        score = new int[2];
        scoreText[0].text = "0";
        scoreText[1].text = "0";
    }
	
    public void OnScoreGoal (int player)
    {
        score[player] += scorePerGoal;
        scoreText[player].text = score[player].ToString();
        Debug.Log("Player" + player + ": " + score[player]);
        

        if(score[player] >= 10)
        {
            //it is winrar.


            WinnerText = Instantiate(TextPrefab) as GameObject;
            WinnerText.transform.parent = Canvas.transform;
            Text txt = WinnerText.GetComponent<Text>();

            int PlayerWin = 0;

            switch(player)
            {
                case 0:
                    PlayerWin = 2;
                    break;
                case 1:
                    PlayerWin = 1;
                    break;
            }

            txt.text = string.Format("Player {0} wins. \n Press left click to reset.", PlayerWin);
            WinnerText.transform.localPosition = Vector3.zero; //for some reason it's putting itself bottom left corner ??
            GameManager.GetInstance().SetGameState(GameManager.GameState.Over);

        }
    }
	// Update is called once per frame
	void Update () {

	
	}
}
