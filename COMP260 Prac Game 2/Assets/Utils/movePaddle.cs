	using UnityEngine;
	using System.Collections;
    
public class movePaddle: MonoBehaviour {
	
	private Rigidbody rigidbody;
	public float speed = 10f;
	public float force = 20f;

	private Vector3 GetMousePosition() {
		Ray ray = Camera.main.ScreenPointToRay (Input.mousePosition);
		Plane plane = new Plane (Vector3.forward, Vector3.zero);
		float distance = 0;
		plane.Raycast (ray, out distance);
		return ray.GetPoint (distance);
	}


	void Start () {
		rigidbody = GetComponent<Rigidbody> ();
		rigidbody.useGravity = false;

	}		

	void OnDrawGizmos() {
		Gizmos.color = Color.yellow;
		Vector3 pos = GetMousePosition();
		Gizmos.DrawLine (Camera.main.transform.position, pos);

	}


	void FixedUpdate () {
		//float moveHorizontal = Input.GetAxis("Horizontal");
		//float moveVertical = Input.GetAxis("Vertical");
		//Vector3 movement = new Vector3(moveHorizontal, moveVertical);
		//rigidbody.AddForce(movement * force * Time.deltaTime);
	

		Vector3 pos = GetMousePosition ();
		Vector3 dir = pos - rigidbody.position;
		Vector3 vel = dir.normalized * speed;

		float move = speed * Time.fixedDeltaTime;
		float distToTarget = dir.magnitude;
	    if (move > distToTarget) {
			vel = vel * distToTarget / move;
	}
		rigidbody.velocity = vel;
}
}